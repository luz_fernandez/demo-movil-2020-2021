package com.co.experitest.properties;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.co.experitest.utilidades.Constantes;

/**
 * @serial Clase que realiza el cargue del archivo de propiedades
 * @author Softesting
 * @version 1.1
 */
public class ConexionProperties {
	
	 public static Properties property;
	 private static FileInputStream fs;
	
	// metodo que realiza el cargue del archivo de propiedades
		public Properties leerArchivoProperties() {
			try {
				fs = new FileInputStream(System.getProperty("user.dir") + Constantes.RUTA_ARCHIVO_CONFIG);
				property = new Properties();
				property.load(fs);

			} catch (IOException e) {
				e.getMessage();
			}
			return property;
		}

}
