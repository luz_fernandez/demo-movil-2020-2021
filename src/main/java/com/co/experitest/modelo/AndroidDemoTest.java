package com.co.experitest.modelo;

import java.net.URL;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.DeviceRotation;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.co.experitest.controlador.ControladorConfiguraciones;
import com.co.experitest.vista.PantallaReporte;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;

public class AndroidDemoTest {

    protected AppiumDriver<MobileElement> driver = null;

    public void test() {
        PantallaReporte caso = new PantallaReporte();

        try {

            DesiredCapabilities dc = new DesiredCapabilities();
            dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".LoginActivity");
            dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.experitest.ExperiBank");
            dc.setCapability("udid", "197ae2c3");
            dc.setCapability("platformName", "Android");
            dc.setCapability("platformVersion", "9.0.0");
            URL url;
            url = new URL("http://127.0.0.1:4723/wd/hub");
            
            System.out.println(dc);
            
            driver = new AppiumDriver<MobileElement>(url, dc);
            if (driver.findElement(By.id("android:id/button1")).getText().equalsIgnoreCase("Aceptar")) {
				driver.findElement(By.id("android:id/button1")).click();
			}
            Thread.sleep(5000);
            driver.findElement(By.id("usernameTextField")).sendKeys("Company");
            Thread.sleep(5000);
            driver.findElement(By.id("passwordTextField")).sendKeys("Company");
            Thread.sleep(5000);
            driver.findElement(By.id("loginButton")).click();
            Thread.sleep(4000);

            Thread.sleep(4000);
            driver.findElement(By.id("makePaymentButton")).click();
            Thread.sleep(2000);
            driver.findElement(By.id("phoneTextField")).sendKeys("123456");
            Thread.sleep(2000);
            driver.findElement(By.id("nameTextField")).sendKeys("Softesting");
            Thread.sleep(2000);
            driver.findElement(By.id("amount")).sendKeys("10");
            Thread.sleep(2000);
            driver.findElement(By.id("countryTextField")).sendKeys("Bogota DC");
            Thread.sleep(2000);
            driver.hideKeyboard();
            Thread.sleep(2000);
            driver.findElement(By.id("sendPaymentButton")).click();
            Thread.sleep(2000);

            MobileElement el1 = (MobileElement) driver.findElementById("android:id/button1");
            el1.click();

            tearDown();

            int resp = JOptionPane.showConfirmDialog(null, "¿Desea generar el reporte?", "Generar Reporte", JOptionPane.YES_NO_OPTION);
            if (resp == JOptionPane.YES_OPTION) {
                caso.abrirarchivo("C:\\Users\\Usuario\\Documents\\Minka\\Mobil\\reporte.html");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en la conexion", "Error", JOptionPane.ERROR_MESSAGE);
            caso.abrirarchivo("C:\\Users\\Usuario\\Documents\\Minka\\Mobil\\reporte.html");
        }

    }

    public void tearDown() {
        driver.quit();
    }

}
