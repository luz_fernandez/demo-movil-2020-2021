package com.co.experitest.modelo;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.text.Element;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.co.experitest.controlador.ControladorConfiguraciones;
import com.co.experitest.utilidades.CapturaPantalla;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class LoginTest {
	public static AndroidDriver<AndroidElement> driver = null;

	String browser = ControladorConfiguraciones.browser;
	URL url;
	DesiredCapabilities dc = new DesiredCapabilities();
	String urlSispos = ControladorConfiguraciones.ambiente;
	String urlRegistroC = ControladorConfiguraciones.ambienteRegistroClientes;
	String usuario = ControladorConfiguraciones.usuario;
	String password = ControladorConfiguraciones.password;
	String nombrePagina = "";

//Caso 1

	@Given("^Ingreso a la aplicacion de eriBank")
	public void El_usuario_se_encuentra_en_la_pagina_erikbank() {

		try {
			dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.experitest.ExperiBank");
			dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".LoginActivity");
			//dc.setCapability("deviceName", "197ae2c3"); // android 28 - 9
			dc.setCapability("deviceName", "86581b77e81700000000"); // andrid 23 - 6
			dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			dc.setCapability("noReset", true);
			dc.setCapability("fullReset", false);
			dc.setCapability("noSign", true);
			url = new URL("http://127.0.0.1:4723/wd/hub");
			this.driver = new AndroidDriver<AndroidElement>(url, dc);
			CapturaPantalla.capturarPantalla(this.driver);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@And("^Ingresa usuario$")
	public void ingreso_usuario() {

		try {
			driver.findElement(By.id("usernameTextField")).sendKeys(usuario);
			CapturaPantalla.capturarPantalla(driver);
		} catch (NoSuchElementException e) {
			CapturaPantalla.capturarPantalla(this.driver);
			Assert.fail("No se puede ingresar a la aplicacion");
			
		}
	}

	@And("^Ingresa password$")
	public void ingresa_password() {
		try {
			driver.findElement(By.id("passwordTextField")).sendKeys(password);
			CapturaPantalla.capturarPantalla(this.driver);
		} catch (NoSuchElementException e) {
			// CapturaPantalla.capturarPantalla("01_Login_ingresar_a_la_url_SISPOS", "0");
			Assert.fail("No se puede ingresar a la aplicacion");
		}
	}

	@When("^Pulsa el boton enviar$")
	public void pulsa_boton_enviar() throws InterruptedException {
		CapturaPantalla.capturarPantalla(this.driver);
		driver.findElement(By.id("loginButton")).click();
        Thread.sleep(4000);
	}

// Caso 2

	@Given("^Usuario pulsa el boton MakePayment")
	public void El_usuario_pulsa_boton_makepayment() {
		try {
            Thread.sleep(4000);
			CapturaPantalla.capturarPantalla(this.driver);
			AndroidElement elemento = driver.findElement(By.id("makePaymentButton"));
			elemento.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@And("^Usuario ingresa el telefono$")
	public void ingreso_telefono() {
		try {
			driver.findElement(By.id("phoneTextField")).sendKeys("123456");
			CapturaPantalla.capturarPantalla(this.driver);

		} catch (NoSuchElementException e) {
			// CapturaPantalla.capturarPantalla("01_Login_ingresar_a_la_url_SISPOS", "0");
			Assert.fail("No se puede ingresar a la aplicacion");
		}
	}

	@And("^Usuario ingresa el nombre$")
	public void ingreso_nombre() {
		try {
			driver.findElement(By.id("nameTextField")).sendKeys("Softesting");
			CapturaPantalla.capturarPantalla(this.driver);
		} catch (NoSuchElementException e) {
			// CapturaPantalla.capturarPantalla("01_Login_ingresar_a_la_url_SISPOS", "0");
			Assert.fail("No se puede ingresar a la aplicacion");
		}
	}

	@And("^Usuario ingresa el monto$")
	public void ingreso_monto() {
		try {
			driver.findElement(By.id("amount")).sendKeys("10");
			CapturaPantalla.capturarPantalla(this.driver);
		} catch (NoSuchElementException e) {
			// CapturaPantalla.capturarPantalla("01_Login_ingresar_a_la_url_SISPOS", "0");
			Assert.fail("No se puede ingresar a la aplicacion");
		}
	}

	@And("^Usuario ingresa la ciudad origen$")
	public void ingreso_ciudad() {
		try {
			driver.findElement(By.id("countryTextField")).sendKeys("Bogota DC");
			Thread.sleep(2000);
			CapturaPantalla.capturarPantalla(this.driver);
		} catch (Exception e) {
			// CapturaPantalla.capturarPantalla("01_Login_ingresar_a_la_url_SISPOS", "0");
			Assert.fail("No se puede ingresar a la aplicacion");
		}
	}

	@When("^Usuario pulsa el boton enviar$")
	public void El_usuario_pulsa_boton_enviar() throws InterruptedException {
		AndroidElement el1 = driver.findElementById("sendPaymentButton");
		el1.click();
		CapturaPantalla.capturarPantalla(this.driver);
		Thread.sleep(2000);
		CapturaPantalla.capturarPantalla(this.driver);		
		driver.findElementById("android:id/button1").click();

         
	}

	@Given("^El usuario finaliza sesion")
	public void El_usuario_finaliza_sesion() {
		try {
			CapturaPantalla.capturarPantalla(this.driver);
			driver.findElement(By.id("logoutButton")).click();			
			tearDown();
			CapturaPantalla.capturarPantalla(this.driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void tearDown() {
		driver.quit();
	}

}
