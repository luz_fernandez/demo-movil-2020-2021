package com.co.experitest.utilidades;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import com.co.experitest.controlador.ControladorConfiguraciones;


public class Sistema {
	
	public static String path;
	public static String path2;
	public static Screen screen;
	
	public Sistema() {

		screen = new Screen();
		path= ControladorConfiguraciones.path+"\\"+ControladorConfiguraciones.nombreReporte+"\\"+ControladorConfiguraciones.nombreReporte+"_"+"evidencias\\"+CapturaPantalla.fechaSistema()+"_"+CapturaPantalla.horaSistema();
		path2 = ControladorConfiguraciones.path+"\\"+ControladorConfiguraciones.nombreReporte+"\\"+ControladorConfiguraciones.nombreReporte+"_"+"reporte_y_evicendias\\";		
	}
	
	public static void findBySikuli(String imgPath, String imgName, boolean clic, int upPix, WebDriver driver) {
		try {
			Pattern p = new Pattern(imgPath).similar((float) 0.8);
			screen.exists(p);
			Region r = screen.find(p);
			r.highlight(2);
			//CapturaPantalla.capturarPantallaResaltada(imgName, "", driver, r.x, r.y - upPix, r.w, r.h);
			if (clic) {
				screen.find(p);
				screen.click(p);
				screen.click(p);
				screen.click();
			}
		} catch (Exception e) {
			e.printStackTrace();
//			for (int i = 0; i < e.getStackTrace().length; i++) {
//				System.out.println(e.getStackTrace()[i].toString());
//			}
//			findBySikuli("src/imgs/home.png", "homeSicom", true, 55);

		}
	}

	public void creaarCarpetaNombreReporte() {
		File newFolder = new File(ControladorConfiguraciones.path+"\\"+ControladorConfiguraciones.nombreReporte);
		newFolder.mkdirs();
	}

}
