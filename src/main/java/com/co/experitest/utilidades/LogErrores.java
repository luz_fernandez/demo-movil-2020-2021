package com.co.experitest.utilidades;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.*;

import com.co.experitest.controlador.Usuario;

/**
 * @serial Clase LogErrores
 * @author Softesting
 * @Version 1.1
 * @since 1/11/2019
 * 
 */
public class LogErrores {

	Date fecha = new Date();
	String usr = System.getProperty("user.name");

	/**
	 * @param error
	 * @param ubicacionArchivoError
	 * @return
	 */
	public boolean creaArchivoLog(String error, String ubicacionArchivoError) {
	
		Logger log = Logger.getLogger(LogErrores.class);
		SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
		String fechaAc = formato.format(fecha);
		System.out.println(usr.toUpperCase() + "_" + fechaAc);
		PatternLayout defaultLayout = new PatternLayout("Tipo : %p, Fecha : %d{dd-MM-yyyy/HH:mm:ss} , Mensaje :%m%n");
		RollingFileAppender rollingFileAppender = new RollingFileAppender();
		try {
			rollingFileAppender.setFile(ubicacionArchivoError + File.separator + usr.toUpperCase() + "_" + fechaAc + ".txt", true, false,
					0);
		} catch (IOException e) {
			e.printStackTrace();
		}
                
                
		rollingFileAppender.setLayout(defaultLayout);
		log.removeAllAppenders();
		log.addAppender(rollingFileAppender);
		log.setAdditivity(false);
		log.error(error);
		
		Usuario.patherrores = ubicacionArchivoError + File.separator + usr.toUpperCase() + "_" + fechaAc + ".txt";
				
		return false;
	}

}
