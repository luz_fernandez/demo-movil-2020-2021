package com.co.experitest.utilidades;

/**
 * @serial Clase constante
 * @author Softesting
 * @Version 1.1
 * @since 2/11/2019
 * 
 */

public final class Constantes {
      
    public static final String RUTA_ARCHIVO_CONFIG = "\\src\\main\\java\\com\\co\\experitest\\properties\\config.properties";
    public static final String RUTA_PAQUETE =  "com.co.experitest.modelo";
    public static final String RUTA_FORMATTER = "com.cucumber.listener.ExtentCucumberFormatter:"; 
    public static final String RUTA_FEATURE =  "src/main/java/resources/features";
    
    public static final String MENSAJE_OPCION_DOS = "Para ejecutar un caso favor seleccione la opcion 1 , y seleccione el caso que desea correr.";
    
    
    
    
}
