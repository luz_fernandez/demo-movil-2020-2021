package com.co.experitest.utilidades;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.co.experitest.properties.ConexionProperties;

/**
 * @serial Clase LeerArchivo lee el csv ubicado en la ruta dada
 * para la extracion de la informacion
 * @author Softesting
 * @Version 1.1
 * @since 2/11/2019
 * 
 */
public class LeerArchivo extends ConexionProperties{

    
	@SuppressWarnings("resource")
	public static List<String> leercsv() throws IOException{
             ConexionProperties leerPropiedades = new ConexionProperties();
             Properties propiedad = leerPropiedades.leerArchivoProperties();
		String csvFile = propiedad.getProperty("rutaCsv");
		String line = "";
		
		String cvsSplitBy = ";";
		
		ArrayList<String> values = new ArrayList<String>();  
		BufferedReader br = new BufferedReader(new FileReader(csvFile));
	    while((line = br.readLine()) != null) {
	    	String[] datos = line.split(cvsSplitBy);
	    	values.addAll(Arrays.asList(datos));
	    }	     
	    return values;
	}
}