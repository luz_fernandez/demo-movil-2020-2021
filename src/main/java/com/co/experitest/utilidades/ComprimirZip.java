package com.co.experitest.utilidades;


import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.co.experitest.controlador.Usuario;


public class ComprimirZip {
	
	public String extension = "";
	public String ultimo = "";
	
	

	public void metodoLocalReporte(String path, String nombreReporte) throws IOException {
		
			
		String files;
		System.out.println("------>"+ path + "\\" + nombreReporte + "\\");
		File archivo = new File(path + "\\" + nombreReporte + "\\" );
		File[] listOfFiles = archivo.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			
			System.out.println("hay"+ " "+ listOfFiles[i].getName());		
			if (listOfFiles[i].isFile()) {

				//String extension = "";
				for (int j = 0; j < listOfFiles[i].getName().length(); j++) {

					// obtiene la extensi�n del archivo
					if (listOfFiles[i].getName().charAt(j) == '.') {

						extension = listOfFiles[i].getName().substring(j, (int) listOfFiles[i].getName().length());
						files = listOfFiles[i].getName();
						if (files.endsWith(".html") || files.endsWith(".HTML")) {

							byte[] buffer = new byte[1024];
							FileOutputStream fos = new FileOutputStream(path + "//" + nombreReporte + "//" + nombreReporte+"_reporte_y_evicendias" + "//" + ""+nombreReporte+"_reporte_"
									+ CapturaPantalla.fechaSistema() + "_" + CapturaPantalla.horaSistema() + ".zip");
							System.out.println("---------->"+ path + "//" + nombreReporte + "//" + nombreReporte+"_reporte_y_evicendias" + "//" + ""+nombreReporte+"_reporte_"
									+ CapturaPantalla.fechaSistema() + "_" + CapturaPantalla.horaSistema() + ".zip");
							ZipOutputStream zos = new ZipOutputStream(fos);
							ZipEntry ze = new ZipEntry(files);
							zos.putNextEntry(ze);
							FileInputStream in = new FileInputStream(path + "//" + nombreReporte + "//" +"reporte" + ".html");
							System.out.println("HOLLLLAAAAAAA--"+path + File.separator +nombreReporte + File.separator +"reporte" + ".html");
							int len;
							while ((len = in.read(buffer)) > 0) {
								zos.write(buffer, 0, len);
							}
							in.close();
							zos.closeEntry();
							zos.close();
							System.out.println("Hecho reporte");
						}
					}
				}
			}
		}
	}
	
	
	public void eliminarPorExtension(String path, String nombreReporte) {
		String sourcePath = path + File.separator +nombreReporte + File.separator+ nombreReporte+"_reporte_y_evicendias";
		//String pathOld = sourcePath.replace("/", "//");
		File prueba = new File(sourcePath);
		File[] ficheros = prueba.listFiles();
		File f = null;
		if (prueba.exists()) {
			for (int x = 0; x < ficheros.length; x++) {
				f = new File(ficheros[x].toString());
				f.delete();
			}
		} else {
			System.out.println("No existe el directorio para eliminar");
		}
	}
	
	public void buscarEvidenciasLocal(String path , String nombreReporte) throws Exception {

		FileFilter filtro = new FileFilter() {
			public boolean accept(File arch) {
				return arch.isDirectory();
			}
		};
		File carpeta = new File(path + File.separator+ nombreReporte +File.separator+ nombreReporte+"_evidencias/");
		File[] archivos = carpeta.listFiles(filtro);
		String Nombre = "";
		if (archivos == null || archivos.length == 0) {
			System.out.println("No hay elementos dentro de la carpeta actual");
			return;
		} else {
			//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			for (int i = 0; i < archivos.length; i++) {
				File archivo = archivos[i];
				ultimo = archivo.getName() + "-" + archivo.lastModified();
				Nombre = archivo.getName();
			}
			// Ubicacion carpeta capturas pantalla
			String parent = path + File.separator + nombreReporte+ File.separator +nombreReporte+"_evidencias/" + Nombre + "/" + "archivo";
			File archivo2 = new File(parent);
			// Ubicacion donde se quiere mover carpeta
			String destino2 = path + File.separator +nombreReporte+File.separator +nombreReporte+"_reporte_y_evicendias"+ File.separator + ""+nombreReporte+"_evidencias_" + CapturaPantalla.fechaSistema() + "_"
					+ CapturaPantalla.horaSistema();
			String destino = destino2 + ".zip";
			
			Usuario.pathevidencias = destino;
			
			comprimir(archivo2.getParent(), destino);
			System.out.println("Hecho Evidencias");
		}
	}
	
	public static void comprimir(String archivo, String archivoZIP) throws Exception {
		ZipOutputStream zip = new ZipOutputStream(new FileOutputStream(archivoZIP));
		agregarCarpeta("", archivo, zip);
		zip.flush();
		zip.close();
	}	
	
	
	public static void agregarCarpeta(String ruta, String carpeta, ZipOutputStream zip) throws Exception {
		File directorio = new File(carpeta);
		for (String nombreArchivo : directorio.list()) {
			if (ruta.equals("")) {
				agregarArchivo(directorio.getName(), carpeta + "/" + nombreArchivo, zip);
			} else {
				agregarArchivo(ruta + "/" + directorio.getName(), carpeta + "/" + nombreArchivo, zip);
			}
		}
	}

	@SuppressWarnings("resource")
	public static void agregarArchivo(String ruta, String directorio, ZipOutputStream zip) throws Exception {
		File archivo = new File(directorio);
		if (archivo.isDirectory()) {
			agregarCarpeta(ruta, directorio, zip);
		} else {
			byte[] buffer = new byte[4096];
			int leido;
			FileInputStream entrada = new FileInputStream(archivo);
			zip.putNextEntry(new ZipEntry(ruta + "/" + archivo.getName()));
			while ((leido = entrada.read(buffer)) > 0) {
				zip.write(buffer, 0, leido);
			}
		}
	}
	
}