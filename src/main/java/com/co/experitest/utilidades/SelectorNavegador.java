package com.co.experitest.utilidades;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;

public class SelectorNavegador {

	private static WebDriver webDriver;

	// M�todo que retorna el web Driver
	@SuppressWarnings("deprecation")
	public static WebDriver open(String browserType) {

		if (webDriver == null) {
			try {
				 if (browserType.equalsIgnoreCase("explorer")) {
					ArrayList<String> output = new ArrayList<String>();
					Process p = Runtime.getRuntime()
							.exec("reg query \"HKLM\\Software\\Microsoft\\Internet Explorer\" /v svcVersion");
					BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()), 8 * 1024);
					BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
					String s = null;
					while ((s = stdInput.readLine()) != null)
						output.add(s);
					String internet_explorer_value = (output.get(2));
					String version = internet_explorer_value.trim().split("   ")[2];
					String sSubCadena = version.substring(0, 3);

					if (sSubCadena.trim().equalsIgnoreCase("10")) {
						System.out.println("INTERNET EXPLORER 10");
						System.setProperty("webdriver.ie.driver", "src/resources/driversBrowser/IEDriverServerRR.exe");
					} else if (sSubCadena.trim().equalsIgnoreCase("11")) {
						 selectDriver();
					} else {
						System.setProperty("webdriver.ie.driver", "src/resources/driversBrowser/IEDriverServerTest.exe");
					}

					InternetExplorerOptions options = new InternetExplorerOptions();
					options.introduceFlakinessByIgnoringSecurityDomains();
					options.setCapability("ignoreZoomSetting", true);
					options.setCapability("requireWindowFocus", true);
					options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
					options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
					options.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
					webDriver = new InternetExplorerDriver(options);
					webDriver.manage().window().maximize();
					webDriver = new InternetExplorerDriver(options);
					webDriver.manage().window().maximize();
				}

			} catch (Exception e) {
				System.out.println("No se conecto al IE");
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Verifique que el navegador seleccionado este instalado.");
				System.exit(0);
			}

		}
		return webDriver;
	}

	private static void selectDriver() {

		String driverTest = "src/resources/driversBrowser/IEDriverServerTest.exe";
		String driverPrepro = "src/resources/driversBrowser/IEDriverServerTest.exe";
		String driverQa = "src/resources/driversBrowser/IEDriverServerTest.exe";

		String driver = "";

		switch (System.getProperty("env")) {
		case "test":
			driver = driverTest;
			break;
		case "qa":
			driver = driverTest;

			break;
		case "prepro":
			driver = driverPrepro;
			break;
		default:
			driver = driverTest;
			break;
		}
		System.setProperty("webdriver.ie.driver", driver);
	}

}
