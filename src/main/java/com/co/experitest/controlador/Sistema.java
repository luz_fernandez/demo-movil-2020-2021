package com.co.experitest.controlador;


import java.io.File;

import com.co.experitest.utilidades.CapturaPantalla;


/**
 * @serial Clase Sistema creacion en el la ruta usuario del sistema
 * carpetas de evidencias
 * @author Softesting
 * @Version 1.1
 * @since 2/11/2019
 * 
 */
public class Sistema {
	
	public static String path;
	public static String path2;
	
	public Sistema() {				
		path= Usuario.path+"\\"+Usuario.nombreReporte+"\\"+Usuario.nombreReporte+"_"+"evidencias\\"+CapturaPantalla.fechaSistema()+"_"+CapturaPantalla.horaSistema();	
		path2 = Usuario.path+"\\"+Usuario.nombreReporte+"\\"+Usuario.nombreReporte+"_"+"reporte_y_evicendias\\";		
	}

	public void creaarCarpetaNombreReporte() {
		File newFolder = new File(Usuario.path+"\\"+Usuario.nombreReporte);
		newFolder.mkdirs();
	}
        
}
