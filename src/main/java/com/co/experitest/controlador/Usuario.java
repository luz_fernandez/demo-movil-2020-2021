package com.co.experitest.controlador;

/**
 * @serial Clase Usuario para el mapeo de la data
 * @author Softesting
 * @Version 1.1
 * @since 2/11/2019
 * 
 */
public class Usuario {
	
	public static String usuario;
	public static String password;
	public static String path;
	public static String browser;
	public static String nombreReporte;
	public static String numeroCaso;
	public static String pathreporte;
	public static String pathevidencias;
	public static String patherrores;
	public static String pathcontador;
	
	public static int acumulador;
	public static int contadorCasos;

	public static int getContadorCasos() {
		return contadorCasos;
	}

	public static void setContadorCasos(int contadorCasos) {
		Usuario.contadorCasos = contadorCasos;
	}
}
