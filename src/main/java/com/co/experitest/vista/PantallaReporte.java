package com.co.experitest.vista;



import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.co.experitest.controlador.Usuario;

public class PantallaReporte extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JLabel reporte;
	private JLabel nombreReporte;
	private JTextField pathbox;
	private JTextField txtNombreReporte;
	private JFileChooser archivo;
	private JButton selector;
	private JButton aceptar;

	public PantallaReporte() {
		super();
		configurarVentana();
		inicializarComponentes();

	}

	private void configurarVentana() {
		this.setTitle("DEMO SOFTESTING");
		this.setSize(360, 370);
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private void inicializarComponentes() {

		reporte = new JLabel();
		nombreReporte = new JLabel();
		archivo = new JFileChooser();
		selector = new JButton();
		aceptar = new JButton();
		pathbox = new JTextField();
		txtNombreReporte = new JTextField();

		// NOMBRE REPORTE
		nombreReporte.setText("Nombre Reporte");
		nombreReporte.setBounds(70, 30, 250, 25);

		txtNombreReporte.setBounds(70, 60, 210, 25);

		txtNombreReporte.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (txtNombreReporte.getText().length() >= 50) // limit to 3
																// characters
				{
					e.consume();
				}
			}
		});

		// PATH REPORTE
		reporte.setText("Seleccione la ubicaci�n del reporte ");
		reporte.setBounds(70, 100, 250, 25);

		selector.setText("...");
		selector.setBounds(70, 130, 25, 25);
		pathbox.setBounds(100, 130, 179, 25);
		pathbox.setEditable(false);

		archivo.setCurrentDirectory(new java.io.File("."));
		archivo.setDialogTitle("Seleccione un directorio");
		archivo.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		//
		aceptar.setText("Aceptar");
		aceptar.setBounds(100, 300, 150, 30);
		//
		selector.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				int returnValue = archivo.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File selectedFile = archivo.getSelectedFile();
					Usuario.path = selectedFile.getAbsolutePath();
					pathbox.setText(Usuario.path);
				}
			}

		});

		aceptar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String nombreReporte = limpiarCaracteres(txtNombreReporte.getText());
				if (nombreReporte.length() > 0) {
					if (Usuario.path != null) {
						Usuario.nombreReporte = nombreReporte;
						MiHilo.mainTest();
						dispose();

					} else {
						JOptionPane.showMessageDialog(null, "Por favor escoger una ruta para guardar el reporte");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Por favor ingrese un nombre para el reporte.");
				}

			}
		});
		//
		this.add(reporte);
		this.add(nombreReporte);
		this.add(archivo);
		this.add(selector);
		this.add(aceptar);
		this.add(pathbox);
		this.add(txtNombreReporte);
	}

	public String limpiarCaracteres(String valor) {
		valor = valor.replaceAll("[^\\dA-Za-z]", "");
		valor = valor.replaceAll("[\\W]|_", "");
		valor = valor.replaceAll("[^a-zA-Z0-9]", "");
		return valor;
	}

	public static void main(String arg[]) {

		PantallaReporte reporte = new PantallaReporte();
		reporte.setVisible(true);

	}
	
	
    public void abrirarchivo(String archivo) {

        try {

            File objetofile = new File(archivo);
            Desktop.getDesktop().open(objetofile);

        } catch (IOException ex) {

            System.out.println(ex);

        }

    }

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
