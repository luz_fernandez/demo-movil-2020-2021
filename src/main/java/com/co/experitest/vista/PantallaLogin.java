package com.co.experitest.vista;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.co.experitest.controlador.ControladorConfiguraciones;
import com.co.experitest.controlador.Sistema;
import com.co.experitest.controlador.Usuario;
import com.co.experitest.utilidades.Constantes;
import com.co.experitest.utilidades.LeerArchivo;
import com.cucumber.listener.ExtentProperties;
import cucumber.api.cli.Main;

class MiHilo extends Thread {
	@Override
	public void run() {
		try {

			// Configuraciom del reporte
			ExtentProperties extentProperties = ExtentProperties.INSTANCE;
			extentProperties.setProjectName("Reporte Pruebas automatizadas Mobile Softesting");
			extentProperties.setReportPath(ControladorConfiguraciones.ubicacionReporte + "//" + ControladorConfiguraciones.nombreReporte + "/reporte.html");
			// Creacion de las carpetas de evidencias y capturas
			new Sistema();
			File newFolder2 = new File(Sistema.path);
			newFolder2.mkdirs();
			File newFolder3 = new File(Sistema.path2);
			newFolder3.mkdirs();
			// Consulta los casos que hay en el CSV
			String[] datos = {};
			List<String> where = new ArrayList<String>();
			for (String a : LeerArchivo.leercsv()) {
				datos = a.split(",");
				String caso = datos[0];
				if (!caso.trim().equalsIgnoreCase("caso")) {
					String run = reemplazarCaso(caso);
					where.add(run);
				}
			}
			String t = "";
			for (int i = 0; i < where.size(); i++) {
				t += ((i > 0) ? "," : "") + where.get(i);
			}
			// Run de los casos por medio de cucumber
			ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
			System.out.println();
			Main.run(new String[] { "-g", Constantes.RUTA_PAQUETE, "-p", Constantes.RUTA_FORMATTER, "-t", t,
					Constantes.RUTA_FEATURE }, contextClassLoader);

		} catch (IOException e) {
			e.getMessage();
		}
		
		ArrayList<String> values;
		
		try {
			values = (ArrayList<String>) LeerArchivo.leercsv();
			int total_casos = values.size();
			JOptionPane.showMessageDialog(null, "Mensaje",
					"Finalizo " + "( " + total_casos + ")" + " " + "de la automatizacion.", JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException e) {
			e.getMessage();
		}
	}


	@SuppressWarnings("unused")
	private void CerrarDriver() {
		try {
			Runtime.getRuntime().exec("C:\\test\\CerrarProceso.bat");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	public static void mainTest() {
		try {
			Thread hiloA = new Thread(new MiHilo(), "hiloA");
			hiloA.start();
		} catch (Throwable e) {
			e.getMessage();
		}
	}

	public String reemplazarCaso(String caso) {
		String after = caso;
		String reeplazo;
		if (caso.trim().equalsIgnoreCase("todoPrestacionesEconomicas")) {
			reeplazo = "@" + caso;
		} else {
			reeplazo = "@Caso" + caso;
		}
		String runnerClase = caso.replace(after, reeplazo);
		return runnerClase;
	}

}
